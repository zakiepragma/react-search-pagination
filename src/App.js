import React from "react";
import UserLists from "./components/UserLists";

const App = () => {
  return (
    <div>
      <UserLists />
    </div>
  );
};

export default App;
